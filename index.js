// note:  yearOfBirth = thisYear - age  may give an incorrect result, can't fix this without more data
//  example: today is June 1st 2020, employee was born in July 1st 1990, expected: 29 years, actual: 30 years
// added undocumented fallbacks behavior for not specified timeInMonth, job, company

const getEmployeeDataMock = () => new Promise((resolve, reject) => {
  const hardcodedTimeout = 2000;
  setTimeout(() => resolve({
    name: 'Василий',
    age: 25,
    job: {
      company: 'Открытая Школа',
      title: 'Разработчик'
    },
    expertise: [
      {
        workName: "EdCrunch",
        timeInMonth: 36
      },
      {
        workName: "Баду",
        timeInMonth: 20
      },
    ]
  }), hardcodedTimeout)
});

getEmployeeDataMock()
  .then(({ age, name, job, expertise }) => {
    const thisYear = (new Date()).getFullYear();
    const yearOfBirth = thisYear - age;

    const totalExpertiseMonths = expertise
      .reduce((sum, expertisePiece) => sum + (expertisePiece.timeInMonth || 0), 0);
    const totalExpertiseFullYears = Math.floor(totalExpertiseMonths / 12);
    let totalExpertiseSuffix = (totalExpertiseFullYears % 10 === 1 && totalExpertiseFullYears !== 11) ?
      "года" : "лет";

    const jobSummary = "сейчас " + (!job ? "не работает" :
        !job.company ? "работает в неизвестной компании" :
        "работает в компании " + job.company);

    const summary = `${name} ${jobSummary} и родился в ${yearOfBirth} году. ` +
      `Его опыт работы более ${totalExpertiseFullYears} ${totalExpertiseSuffix}`;
    console.log(summary);
})
  .catch(error => {
    console.log(error);
  });